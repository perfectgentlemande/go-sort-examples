package main

import "fmt"

func merge(left, right []int) []int {
	size, i, j := len(left) + len(right), 0, 0
	slice := make([]int, size, size)

	for k := 0; k < size; k++ {
		if i > len(left) - 1 && j <= len(right) - 1 {
			slice[k] = right[j]
			j++
		} else if j > len(right) - 1 && i <= len(left) - 1 {
			slice[k] = left[i]
			i++
		} else if left[i] < right[j] {
			slice[k] = left[i]
			i++
		} else {
			slice[k] = right[j]
			j++
		}
	}

	return slice
}

func mergeSortSimple(slice []int) []int {
	if len(slice) < 2 {
		return slice
	}

	mid := len(slice) / 2
	return merge(mergeSortSimple(slice[:mid]), mergeSortSimple(slice[mid:]))
}

func mergeSortConcurrent(slice []int, r chan []int) {
	if len(slice) < 2 {
		r <- slice
		return
	}

	leftChan := make(chan []int)
	rightChan := make(chan []int)
	mid := len(slice)/2

	go mergeSortConcurrent(slice[:mid], leftChan)
	go mergeSortConcurrent(slice[mid:], rightChan)

	lData := <- leftChan
	rData := <- rightChan

	r <- merge(lData, rData)

	close(leftChan)
	close(rightChan)

	return
}

func main() {
	slice := []int{52, 13, 99, 31, 1, 5, 42, 55, 8, 10, 12, 71, 14, 7, 2}
	fmt.Println(slice)

	sortedSimpleSlice := mergeSortSimple(slice)
	fmt.Println(sortedSimpleSlice)

	resChan := make(chan []int)
	go mergeSortConcurrent(slice, resChan)
	sortedConcurrentSlice := <- resChan
	close(resChan)

	fmt.Println(sortedConcurrentSlice)

}